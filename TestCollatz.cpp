// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <sstream>  // istringtstream, ostringstream

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream r("1 10\n");
    int i;
    int j;
    const bool b = collatz_read(r, i, j);
    ASSERT_EQ(b, true);
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);}
    
TEST(CollatzFixture, read_2) {
    istringstream r("33 52\n");
    int i;
    int j;
    const bool b = collatz_read(r, i, j);
    ASSERT_EQ(b , true);
    ASSERT_EQ(i ,  33);
    ASSERT_EQ(j , 52);}
    
TEST(Collatz, read_3) {
    std::istringstream r("69 89\n");
    int i;
    int j;
    const bool b = collatz_read(r, i, j);
    ASSERT_EQ(b , true);
    ASSERT_EQ(i ,  69);
    ASSERT_EQ(j ,  89);}



// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 11);}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 300);}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 411);}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 1900);}
    
TEST(CollatzFixture, eval_5) {
    const int v = collatz_eval(54485, 98054);
    ASSERT_EQ(v, 351);}

TEST(CollatzFixture, eval_6) {
    const int v = collatz_eval(887357, 652959);
    ASSERT_EQ(v, 525);}

TEST(CollatzFixture, eval_7) {
    const int v = collatz_eval(28089, 120501 );
    ASSERT_EQ(v , 354);}

TEST(CollatzFixture, eval_8) {
    const int v = collatz_eval(697904, 774848);
    ASSERT_TRUE(v , 468);}


// -----
// print
// -----

TEST(c, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");}
    
TEST(CollatzFixture, print_1) {
    std::ostringstream w;
    collatz_print(w, 189525, 431167, 449);
    ASSERT_EQ(w.str() , "189525 431167 449\n");}

TEST(CollatzFixture, print_2) {
    std::ostringstream w;
    collatz_print(w, 70390, 108641, 354);
    ASSERT_EQ(w.str() ,"70390 108641 354\n");}

TEST(CollatzFixture, print_3) {
    std::ostringstream w;
    collatz_print(w, 88182, 106090, 341);
    ASSERT_EQ(w.str() ,"88182 106090 341\n");}


// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 11\n100 200 300\n201 210 411\n900 1000 1900\n", w.str());}
    
TEST(CollatzFixture, solve_1) {
    std::istringstream r("922587 734065\n613756 651432\n610185 788565\n");
    std::ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("922587 734065 525\n613756 651432 447\n610185 788565 468\n", w.str());}

TEST(CollatzFixture, solve_2) {
    std::istringstream r("845708 365891\n845708 365891\n697904 774848\n");
    std::ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("845708 365891 525\n845708 365891 525\n697904 774848 468\n", w.str());}

TEST(CollatzFixture, solve_3) {
    std::istringstream r("743488 998440\n990807 536166\n704323 133864\n");
    std::ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("743488 998440 525\n990807 536166 525\n704323 133864 470\n", w.str());}
