// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream

#include "Collatz.hpp"

using namespace std;

// ------------
// collatz_read
// ------------

bool collatz_read (istream& r, int& i, int& j) {
      r >> i;
    if (!r)
        return false;
    r >> j;
    assert(i > 0);
    assert(j > 0);
    return true;
}

// ------------
// collatz_eval
// ------------
    

int cycle(int cache [], int size, int n) {
    int count = 0;
    int value = 0;
    
    if(n < size) {// n already has cycle in cache
        value = cache[n];
    }
    // otherwise loop till known value found or till 1.
    while(value == 0) {
        if((n % 2) == 0) { // n is even
            n = n / 2;
        } else { // n is odd
            n = n + (n >> 1) + 1;
            count++;
        }
        count++;
        
        if(n > 0) {
            // current n in cache?
            if(n < size) {
                value = cache[n];
            }
        } else {
            return n;
        }
    }
    int final_value = value + count
    cache[n] = final_value;
    
    return final_value + count; // final cycle length value
}

// initilize cche by computing the first size cycle lengths
void init_cache (int cache [], int size) {
    cache[1] = 1; 
    for(int i = 2; i < size; i++) {
        cycle(cache, size, i);     
    }
}

int collatz_eval (int i, int j) {
   // Assertions
    assert(i > 0 && j > 0 && i < 1000000 && j < 1000000);
    int max = 0;
    int count = 0;
    
    int size = 10000;
    int main_cache [10000] = {0};
    
    init_cache(main_cache, size);

     if (i > j) {
        int temp = i;
        i = j;
        j = temp;
    }
        
     for (int current = i ; current <= j; current++) {
        count = cycle(main_cache, size, current);
        if (count > max) {
             max = count;
                }
            }

    assert(max > 0);
    return max;        
    
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    int i;
    int j;
    while (collatz_read(r, i, j)) {
        const int v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
         }
}
