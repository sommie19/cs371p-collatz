# CS371p: Object-Oriented Programming Collatz Repo

* Name: Anazodo Somfebechi

* EID: sza262

* GitLab ID: sommie19

* HackerRank ID: (your HackerRank ID)

* Git SHA: (most recent Git SHA, final change to your repo will be adding this value)

* GitLab Pipelines: (link to your GitLab CI Pipeline)

* Estimated completion time: (estimated time in hours, int or float)

* Actual completion time: (actual time in hours, int or float)

* Comments: (any additional comments you have)
